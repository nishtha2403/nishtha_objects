function keys(obj) {
    let keysArr = [];

    for(let key in obj){
        keysArr.push(key);
    }

    return keysArr;
}

module.exports = keys;