function mapObjects(obj,cb) {
     let mappedObj = {};

     for(let key in obj){
         mappedObj[key] = cb(obj[key],key);
     }

     return mappedObj;

}

module.exports = mapObjects;