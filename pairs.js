function pairs(obj) {
    let pairedArr = [];

    for(let key in obj){
        pairedArr.push([key,obj[key]]);
    }

    return pairedArr;

}

module.exports = pairs;