const defaults = require('../defaults');

const testObject = { 
    name: 'Bruce Wayne', 
    age: 36, 
    sex: undefined,
    location: 'Gotham' 
};

const defaultProps = { 
    name: 'Bruce Wayne', 
    age: 36, 
    sex: 'Male',
    location: 'Gotham',
    profession: 'teacher'
};

let filledObj = defaults(testObject,defaultProps);

console.log(filledObj);
