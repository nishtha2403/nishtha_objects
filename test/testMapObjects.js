const mapObjects = require('../mapObjects');

const testObject = { 
    name: 'Bruce Wayne', 
    age: 36, 
    location: 'Gotham' 
};

let output = mapObjects(testObject,(value)=>value+" - modified");

console.log(output);

output = mapObjects(testObject,(value,key) => key+" : "+value);

console.log(output);