const values = require('../values.js');

const testObject = { 
    name: 'Bruce Wayne', 
    age: 36, 
    location: 'Gotham',
    isDriving() {
        return false;
    }
};

console.log(values(testObject));