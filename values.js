function values(obj) {
    let valuesArr = [];

    for(let key in obj){
        if(typeof obj[key] != 'function'){
            valuesArr.push(obj[key]);
        }
    }

    return valuesArr;
}

module.exports = values;